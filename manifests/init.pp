# basic ipsec configuration
#
# this configures all packages and required kernel modules, but
# doesn't configure any host, see `ipsec::network` instead.
#
# when first loaded, this will add a list of modules to the kernel,
# but this will only load on reboot.
class ipsec(
  Enum['present','absent'] $ensure = 'present',
) {
  if $ensure == 'present' {
    $service_ensure   = 'running'
    $directory_ensure = 'directory'
  } else {
    $service_ensure   = 'stopped'
    $directory_ensure = 'absent'
  }
  package { [
    'strongswan',
    'libstrongswan-standard-plugins'
  ]:
    ensure => $ensure,
  }

  service { 'ipsec':
    ensure  => $service_ensure,
    require => Package['strongswan'],
  }

  file {
    default:
      notify  => Service['ipsec'];
    '/etc/ipsec.conf':
      ensure  => $ensure,
      content => template('ipsec/ipsec.conf.erb');
    '/etc/ipsec.secrets':
      ensure  => $ensure,
      mode    => '0400',
      content => template('ipsec/ipsec.secrets.erb');
    '/etc/ipsec.conf.d':
      ensure  => $directory_ensure,
      purge   => true,
      recurse => true,
      mode    => '0555';
    '/etc/ipsec.secrets.d':
      ensure  => $directory_ensure,
      purge   => true,
      recurse => true,
      mode    => '0500';
    '/etc/ipsec.conf.d/00-default.conf':
      ensure  => $ensure,
      content => template('ipsec/ipsec.conf-00-default.conf.erb');
    '/etc/strongswan.d/charon-logging.conf':
      ensure  => $ensure,
      content => template('ipsec/strongswan-charon-logging.conf');
  }

  # TODO managing the apparmor profile should only be done if we can detect
  # that apparmor is installed, but we currently don't have such information
  if $ensure == 'present' {
    file { '/etc/apparmor.d/local/usr.lib.ipsec.charon':
      ensure => $ensure,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/ipsec/ipsec_apparmor_profile'
    }
  }
  else {
    # Remove the local rules. However, apparmor needs the file to stay present
    # otherwise it errors out while parsing the ipsec profile.
    file { '/etc/apparmor.d/local/usr.lib.ipsec.charon':
      ensure   => $ensure,
      owner    => 'root',
      group    => 'root',
      mode     => '0644',
      contents => ''
    }
  }
  exec { 'reload ipsec apparmor profile':
    command     => '/usr/sbin/apparmor_parser -r /etc/apparmor.d/usr.lib.ipsec.charon',
    refreshonly => true,
    subscribe   => File['/etc/apparmor.d/local/usr.lib.ipsec.charon'],
    notify      => Service['ipsec'];
  }

  ferm::rule {
    'ipsec-peers':
      ensure      => $ensure,
      description => 'ipsec protocols are allowed from the ipsec peers',
      domain      => '(ip ip6)',
      chain       => 'ipsec-peers',
      rule        => 'DROP',
      prio        => 'zzz-999';
    'ipsec':
      ensure      => $ensure,
      description => 'ipsec protocols are allowed from the ipsec peers',
      domain      => '(ip ip6)',
      rule        => @(EOF),
                     proto udp dport (isakmp 4500) jump ipsec-peers;
                     proto esp                     jump ipsec-peers
                     | EOF
  }

  # Since we disable module loading after boot, we want to load them all at boot time
  ensure_resource('kmod::load', [
    'af_alg',
    'af_key',
    'ah4',
    'algif_skcipher',
    'ansi_cprng',
    'authenc',
    'drbg',
    'echainiv',
    'esp4',
    'ipcomp',
    'macvlan',
    'macvtap',
    'tunnel4',
    'vhost',
    'vhost_net',
    'xfrm4_tunnel',
    'xfrm_algo',
    'xfrm_ipcomp',
    'xfrm_user',
  ], {
    ensure => $ensure,
  })
}
