# an ipsec "client"
#
# this is an ipsec peer that is not managed by Puppet. or, in other
# words, it is only *one* side of an `ipsec::peer`. Configuration will
# be required on the other end, either through this resource or
# manually. See this documentation for manual configurations:
#
# https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/ipsec#special-case-roaming-clients
#
# @todo this should probably be refactored to be reused by
# ipsec::network and ipsec::peer but this would require configuration
# files (from a single 10-puppet-$NETWORK to one file per peer)
#
# @param ensure present or absent, to remove or create the resource
# @param local_ipaddress the ip address to announce to the other peer,
#                        defaults to `ipaddress` fact
# @param local_networks the ips *networks* to announce, optional
# @param local_id unique name for this peer, defaults to `hostname` fact
# @param peer_ipaddress the remote's IP address, defaults to `%any` to
#                       allow for roaming
# @param peer_networks the remote's network that are allowed to be announced
# @param peer_id a unique name for that peer, reused to declare the secret
# @param peer_ipaddress_firewall the remote's IP address as far as the
#                                firewall is concerned
define ipsec::client(
  Enum['present','absent'] $ensure = 'present',
  # our local configuration, or "left" in the strongswan config
  Stdlib::Host $local_ipaddress = $::ipaddress,
  Array[Stdlib::IP::Address] $local_networks = [],
  String[1] $local_id = $::hostname,

  # our "remote" or "peer", or "right" in the config
  Variant[Stdlib::Host,Enum['%any', '%any6', '%any4']] $peer_ipaddress = '%any',
  Array[Stdlib::IP::Address] $peer_networks = [],
  String[1] $peer_id = $name,
  Optional[Stdlib::Host] $peer_ipaddress_firewall = undef,
) {
  if $ensure == 'present' {
    include ipsec
  }

  # create the data portion for the key derivation function
  #
  # It needs to be the same data on both ends of a connection, so the
  # corresponding secrets entry at the peer gets the same PSK.  We do
  # this by putting the peer's info and our info in some arbitrary,
  # yet canonical order by sorting.
  $psk_name = ($local_id < $peer_id) ? {
    true  => "ipsec::client::psk::${local_id}-${peer_id}",
    false => "ipsec::client::psk::${peer_id}-${local_id}",
  }
  $psk = trocla($psk_name, 'plain')
  file { "/etc/ipsec.secrets.d/20-${peer_id}.secrets":
    ensure  => $ensure,
    content => "${peer_id} : PSK \"${psk}\"\n",
    notify  => Service['ipsec'],
  }

  $leftsubnet = $local_networks ? {
    [] => '',
    default => "leftsubnet = ${$local_networks.join(', ')}"
  }
  $rightsubnet = $peer_networks ? {
    [] => '',
    default => "rightsubnet = ${$peer_networks.join(', ')}"
  }
  file { "/etc/ipsec.conf.d/20-${peer_id}.conf":
    ensure  => $ensure,
    content => @("EOF"),
    conn ${peer_id}
      # left is us (local, chi-node-03)
      left      = ${local_ipaddress}
      ${leftsubnet}
      leftid = ${local_id}
    
      # right is our peer (remote, anarcat's workstation)
      right       = ${peer_ipaddress}
      ${rightsubnet}
      rightid = ${peer_id}
    
      auto=route
    | EOF
    notify  => Service['ipsec'],
  }
  if $peer_ipaddress_firewall {
    ferm::rule { "ipsec-${peer_id}":
      ensure      => $ensure,
      description => "allow ipsec protocols for peer ${peer_id}",
      domain      => '(ip ip6)',
      chain       => 'ipsec-peers',
      rule        => "saddr ${peer_ipaddress_firewall} ACCEPT",
    }
  }
}
